# Terraform Certification - Dicas para o teste 

* Terraform console = Console interativo para testar interpolação do TF

* Usar Vault provider não mascara a secret no plan ou arquivo de state

* Comandos para instalar e atualizar módulos (terraform init && terraform get)

* TF_LOG env var para habilitar log detalhado

* Plugins ficam localizados dentro de .terraform/plugins depois do init

* terraform apply -refresh-only detectar drift e atualizar o arquivo de state

* Paralelismo - Pode executar até 10 recursos ao mesmo tempo no apply

* Publicar módulos públicos para o TF = Github(Public), tags no formato x.y.z e opcionalmente começar com v, módulo precisa sempre ter o formato terraform-provider-name

* Unix não é uma distro suportada pelo TF

* air-gapped instalação quando não se tem internet

* recomendação de 2 espaços de indentação

* Repositório de módulos privados para customização de módulos para a organização (Cloud e Enterprise)

* Clustered Deployment pode usar PostgreSQL como backend

* Recursos exclusivos para o Cloud e Enterprise (Log de auditoria, SAML/SSO e Self-Service IAC)

* aws = "~> 1.2.0" significa versões menores superiores a 1.2.0, exemplo 1.2.1, 1.2.5 até 1.2.9

* terraform workspace new (Criar novo) terraform workspace select (selecionar)

* Versionadores suportados (Bitbucket Cloud, Azure DevOps Center, GitHub Enterprise, GitHub)

* depends_on é uma dependência explícita

* aws_instance.web_server.id é um exemplo de dependência implícita

* TF guarda o local state quando usado workspaces dentro do diretório terraform.tfstate.d

* TF_VAR variável de ambiente para exportar variáveis, exemplo "export TF_VAR_region=us-east-1"

* Terraform pode usar como linguagem opcional o JSON

* Terraform Workspace não possui funcionalidade idêntica nas diferentes versões do TF

* Quais as maneiras de se inicializar o backend (Usando -backend-config=PATH, linha de comando com pares de chave/valor e modo interativo)

* Benefícios do Terraform State (Aumento de performance, mapeamento das configurações para o mundo real e determinar a ordem correta de destruição dos recursos)

* Terraform pode ser gerenciado via CLI mas requer um API Token

* Clustering é um recurso exclusivo da versão Enterprise

* Bloco required_providers é usado para especificar uma versão para o provider 

* Write -> Plan -> Apply Terraform Workflow

* TF_LOG=TRACE é o modo mais "verboso" de logs

* Um workspace pode ser configurado para apenas um VCS repo, porém múltiplos workspaces podem usar o mesmo repo

* Arquivos que devem estar no gitignore (terraform.tfvars e terraform.tfstate)

* Governança e gerenciamento de times é um recurso pago da versão Cloud do TF

* terraform show é usado para inspecionar o arquivo de state do TF

* Tipo de conexões suportadas pelo remote-exec (SSH, WINRM)

* local-exec só deve ser utilizado em último recurso quando o provider não satisfaz o necessário para a demanda

* terraform apply -replace="aws_instance.web" destrói e recria o recurso sem precisar mexer no código

* terraform apply -refresh-only refletir alterações no console para dentro do arquivo de state
